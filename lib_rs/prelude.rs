pub use serial2::{
    CharSize,
    Parity,
    StopBits,
};

pub use crate::{
    timebase::{
        DIM3000,
        SerialSettings,
        FreqSweepMode,
        TBError,
        TBResult,
    },
};

