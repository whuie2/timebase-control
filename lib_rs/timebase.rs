//! Structures for interacting with TimeBase devices over a serial connection.
//!
//! See the manual for more details on individual commands.

#[allow(unused_imports)]

use std::{
    io::{ BufReader, BufRead, Read, Write },
    path::Path,
    str::FromStr,
    time::Duration,
};
// use byteorder::{ ByteOrder, LittleEndian };
use serial2::{
    SerialPort,
    IntoSettings,
};
pub use serial2::{
    CharSize,
    Parity,
    StopBits,
};
use thiserror::Error;

#[derive(Debug, Error)]
pub enum TBError {
    #[error("couldn't connect to TimeBase device: {0}")]
    FailedConnect(String),

    #[error("couldn't set read timeout: {0}")]
    FailedSetReadTimeout(String),

    #[error("couldn't set write timeout: {0}")]
    FailedSetWriteTimeout(String),

    #[error("couldn't parse device response as float: {0}")]
    InvalidRespNumber(String),

    #[error("pulse frequency must be in the range [20, 1000] Hz")]
    PulseFrequencyOutOfRange,

    #[error("pulse duty cycle must be in the range [1, 99] %")]
    PulseDutyOutOfRange,

    #[error("amplitude must be in the range [14, 34] dBm")]
    AmplitudeOutOfRange,

    #[error("frequency must be in the range [10, 400] MHz")]
    FrequencyOutOfRange,

    #[error("frequency step size must be in the range [1e-6, 10] MHz")]
    FrequencyStepOutOfRange,

    #[error("frequency deviation number must be in the range [0, 15]")]
    FrequencyModDevOutOfRange,

    #[error("frequency sweep start must be in the range [10, 400] MHz")]
    FrequencySweepStartOutOfRange,

    #[error("frequency sweep stop must be in the range [10, 400] MHz")]
    FrequencySweepStopOutOfRange,

    #[error("frequency sweep step size must be in the range [10, 400] MHz")]
    FrequencySweepStepOutOfRange,

    #[error("frequency sweep step time must be in the range [4e-3, 262] μs")]
    FrequencySweepStepTimeOutOfRange,

    #[error("device error: {0}")]
    TBDeviceError(String),

    #[error("serial communication error: {0}")]
    SerialCommunicationError(#[from] std::io::Error),
}
pub type TBResult<T> = Result<T, TBError>;

pub const CRLF: &str = "\r\n";

/// Settings for a serial connection.
#[derive(Copy, Clone, Debug)]
pub struct SerialSettings {
    pub baud_rate: u32,
    pub char_size: CharSize,
    pub parity: Parity,
    pub stop_bits: StopBits,
    pub timeout_sec: f64,
}

impl Default for SerialSettings {
    fn default() -> Self {
        return Self {
            baud_rate: 19200,
            char_size: CharSize::Bits8,
            parity: Parity::None,
            stop_bits: StopBits::One,
            timeout_sec: 1.0,
        };
    }
}

impl IntoSettings for SerialSettings {
    fn apply_to_settings(self, settings: &mut serial2::Settings)
        -> Result<(), std::io::Error>
    {
        settings.set_baud_rate(self.baud_rate)?;
        settings.set_char_size(self.char_size);
        settings.set_parity(self.parity);
        settings.set_stop_bits(self.stop_bits);
        return Ok(());
    }
}

/// Main driver type to handle communication with a TimeBase DIM3000 over USB.
///
/// Provided methods are tailored specifically for the DIM3000, but this type
/// can in principle handle other devices as well.
pub struct DIM3000 {
    dev: BufReader<SerialPort>,
}

impl DIM3000 {
    /// Connect to a serial device at the given address, creating a new instance
    /// of `Self`.
    pub fn connect<P>(addr: P, settings: SerialSettings) -> TBResult<Self>
    where P: AsRef<Path>
    {
        let timeout_sec: f64 = settings.timeout_sec;
        let mut dev
            = SerialPort::open(addr, settings)
            .map_err(|e| TBError::FailedConnect(e.to_string()))?;
        dev.set_read_timeout(std::time::Duration::from_secs_f64(timeout_sec))
            .map_err(|e| TBError::FailedSetReadTimeout(e.to_string()))?;
        dev.set_write_timeout(std::time::Duration::from_secs_f64(timeout_sec))
            .map_err(|e| TBError::FailedSetWriteTimeout(e.to_string()))?;
        return Ok(Self { dev: BufReader::new(dev) });
    }

    /// Disconnect, dropping `self`.
    pub fn disconnect(self) { }

    /// Get the read timeout time, in seconds, of the serial connection.
    pub fn get_read_timeout(&self) -> TBResult<f64> {
        return Ok(
            self.dev.get_ref().get_read_timeout()
                .map(|dur| dur.as_secs_f64())?
        );
    }

    /// Get the write timeout time, in seconds, of the serial connection.
    pub fn get_write_timeout(&self) -> TBResult<f64> {
        return Ok(
            self.dev.get_ref().get_write_timeout()
                .map(|dur| dur.as_secs_f64())?
        );
    }

    /// Set the read timeout time, in seconds, of the serial connection.
    pub fn set_read_timeout(&mut self, timeout_sec: f64)
        -> TBResult<&mut Self>
    {
        self.dev.get_mut()
            .set_read_timeout(Duration::from_secs_f64(timeout_sec))
            .map_err(|e| TBError::FailedSetReadTimeout(e.to_string()))?;
        return Ok(self);
    }

    /// Set the write timeout time, in seconds, of the serial connection.
    pub fn set_write_timeout(&mut self, timeout_sec: f64)
        -> TBResult<&mut Self>
    {
        self.dev.get_mut()
            .set_write_timeout(Duration::from_secs_f64(timeout_sec))
            .map_err(|e| TBError::FailedSetWriteTimeout(e.to_string()))?;
        return Ok(self);
    }

    /// Send a raw string of bytes to the device.
    pub fn send_raw(&mut self, cmd: &[u8]) -> TBResult<&mut Self> {
        self.dev.get_mut().write_all(cmd)?;
        return Ok(self);
    }

    /// Send `cmd` to the device, appending newline if not already present.
    pub fn send(&mut self, cmd: &str) -> TBResult<&mut Self> {
        let mut cmd: String = cmd.to_string();
        if !cmd.ends_with(CRLF) { cmd += CRLF; }
        return self.send_raw(cmd.as_bytes());
    }

    /// Read bytes from the device into a `String` until newline or EOF is
    /// reached. The newline character will be included in the returned string.
    pub fn recv(&mut self) -> TBResult<String> {
        let mut buffer = String::new();
        self.dev.read_line(&mut buffer)?;
        return Ok(buffer);
    }

    /// Flush and return all available bytes from the device's output queue.
    pub fn flush(&mut self) -> TBResult<Vec<u8>> {
        let mut buffer: Vec<u8> = Vec::new();
        self.dev.read_to_end(&mut buffer)?;
        return Ok(buffer);
    }

    /// [`send`][Self::send] followed by [`recv`][Self::recv], returning the
    /// response as a string. This method will also call [`flush`][Self::flush]
    /// before sending the `cmd` and return a [`TBError::TBDeviceError`] if
    /// the response is an error message from the device. Leading and trailing
    /// whitespace characters are removed.
    pub fn ask(&mut self, cmd: &str) -> TBResult<String> {
        self.flush()?;
        self.send(cmd)?;
        let response: String = self.recv()?;
        return Ok(response.trim().to_string());
    }

    /// Report information about the device.
    pub fn get_info(&mut self) -> TBResult<String> {
        return self.ask("*IDN?");
    }

    /// Enable or disable RF output.
    pub fn set_output(&mut self, onoff: bool) -> TBResult<&mut Self> {
        self.send(if onoff { "OUT_on" } else { "OUT_off" })?;
        return Ok(self);
    }

    /// Enable or disable pulse mode.
    pub fn set_pulse_mode(&mut self, onoff: bool) -> TBResult<&mut Self> {
        self.send(if onoff { "RFp_on" } else { "RFp_off" })?;
        return Ok(self);
    }

    /// Set the pulse frequency in hertz, [20, 1000].
    pub fn set_pulse_freq(&mut self, freq: f64) -> TBResult<&mut Self> {
        (20.0..=1000.0).contains(&freq).then_some(())
            .ok_or(TBError::PulseFrequencyOutOfRange)?;
        self.send(&format!("RFpfr:{:.0}", freq))?;
        return Ok(self);
    }

    /// Set the pulse duty cycle in percent, [1, 99].
    pub fn set_pulse_duty(&mut self, duty: f64) -> TBResult<&mut Self> {
        (1.0..=99.0).contains(&duty).then_some(())
            .ok_or(TBError::PulseDutyOutOfRange)?;
        self.send(&format!("RFpdt:{:.0}", duty))?;
        return Ok(self);
    }

    /// Get the output amplitude in dBm.
    pub fn get_amplitude(&mut self) -> TBResult<f64> {
        let response: String = self.ask("AMP?")?;
        return response.parse::<f64>()
            .map(|amp| amp / 10.0)
            .map_err(|_| TBError::InvalidRespNumber(response));
    }

    /// Set the output amplitude in dBm, [14, 34].
    pub fn set_amplitude(&mut self, amp: f64) -> TBResult<&mut Self> {
        (14.0..=34.0).contains(&amp).then_some(())
            .ok_or(TBError::AmplitudeOutOfRange)?;
        self.send(&format!("AMP:{:.0}", amp * 10.0))?;
        return Ok(self);
    }

    /// Get the output frequency in MHz.
    pub fn get_frequency(&mut self) -> TBResult<f64> {
        let response: String = self.ask("FRQ?")?;
        return response.parse::<f64>()
            .map(|freq| freq / 1e6)
            .map_err(|_| TBError::InvalidRespNumber(response));
    }

    /// Set the output frequency in MHz, [10, 400].
    pub fn set_frequency(&mut self, freq: f64) -> TBResult<&mut Self> {
        (10.0..=400.0).contains(&freq).then_some(())
            .ok_or(TBError::FrequencyOutOfRange)?;
        self.send(&format!("FRQ:{:.0}", freq * 1e6))?;
        return Ok(self);
    }

    /// Get the frequency step size in MHz.
    pub fn get_frequency_step(&mut self) -> TBResult<f64> {
        let response: String = self.ask("FRQs?")?;
        return response.parse::<f64>()
            .map(|step| step / 1e6)
            .map_err(|_| TBError::InvalidRespNumber(response));
    }

    /// Set the frequency step size in MHz, [1e-6, 10].
    pub fn set_frequency_step(&mut self, step: f64) -> TBResult<&mut Self> {
        (1e-6..=10.0).contains(&step).then_some(())
            .ok_or(TBError::FrequencyStepOutOfRange)?;
        self.send(&format!("FRQs:{:.0}", step * 1e6))?;
        return Ok(self);
    }

    /// Increment the frequency by one step size.
    pub fn frequency_inc(&mut self) -> TBResult<&mut Self> {
        self.send("FRQi")?;
        return Ok(self);
    }

    /// Decrement the frequency by one step size.
    pub fn frequency_dev(&mut self) -> TBResult<&mut Self> {
        self.send("FRQd")?;
        return Ok(self);
    }

    /// Enable or disable frequency modulation mode.
    pub fn set_frequency_mod(&mut self, onoff: bool) -> TBResult<&mut Self> {
        self.send(if onoff { "FM_on" } else { "FM_off" })?;
        return Ok(self);
    }

    /// Set the frequency modulation deviation.
    ///
    /// Inputs are integers 0 through 15 corresponding to frequency ranges
    /// spanning ±Δf where
    /// ```text
    /// Δf = (3200 Hz) * 2^(dev)
    /// ```
    pub fn set_frequency_mod_dev(&mut self, dev: usize) -> TBResult<&mut Self> {
        (0..=15).contains(&dev).then_some(())
            .ok_or(TBError::FrequencyModDevOutOfRange)?;
        self.send(&format!("FMdev:{}", dev))?;
        return Ok(self);
    }

    /// Set the sweep mode.
    ///
    /// See also [`FreqSweepMode`].
    pub fn set_sweep_mode(&mut self, mode: FreqSweepMode)
        -> TBResult<&mut Self>
    {
        self.send(&format!("SSWPm:{}", mode as usize))?;
        return Ok(self);
    }

    /// Set the start and stop frequencies of the sweep in MHz, [10, 400].
    pub fn set_sweep_range(&mut self, f0: f64, f1: f64) -> TBResult<&mut Self> {
        (10.0..=400.0).contains(&f0).then_some(())
            .ok_or(TBError::FrequencySweepStartOutOfRange)?;
        (10.0..=400.0).contains(&f1).then_some(())
            .ok_or(TBError::FrequencySweepStopOutOfRange)?;
        self.send(&format!("SSWPs:{:.0}", f0 * 1e6))?;
        self.send(&format!("SSWPp:{:.0}", f1 * 1e6))?;
        return Ok(self);
    }

    /// Set the frequency sweep step size in MHz, [10, 400].
    pub fn set_sweep_step(&mut self, step: f64) -> TBResult<&mut Self> {
        (10.0..=400.0).contains(&step).then_some(())
            .ok_or(TBError::FrequencySweepStepOutOfRange)?;
        self.send(&format!("SSWPf:{:.0}", step * 1e6))?;
        return Ok(self);
    }

    /// Set the frequency sweep step time in μs, [4e-3, 262].
    ///
    /// Values will be rounded down to the nearest multiple of 4 ns.
    pub fn set_sweep_step_time(&mut self, step_time: f64)
        -> TBResult<&mut Self>
    {
        (4e-3..=262.0).contains(&step_time).then_some(())
            .ok_or(TBError::FrequencySweepStepTimeOutOfRange)?;
        self.send(&format!("SSWPt:{:.0}", step_time * 1e3))?;
        return Ok(self);
    }
}

/// Frequency sweep mode. For use with [`DIM3000::set_sweep_mode`].
#[derive(Copy, Clone, Debug)]
pub enum FreqSweepMode {
    Off = 0,
    TriangleInt = 1,
    TriangleExt = 2,
    SawtoothInt = 3,
    SawtoothExt = 4,
}

